class AddDescriptionToVolume < ActiveRecord::Migration[5.1]
  def change
    add_column :volumes, :description, :string
  end
end
